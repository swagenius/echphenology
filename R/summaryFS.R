##' summaryFS returns standard info about a flowering schedule
##'
##' A great function 
##' 
##' @title summary of a flowernig schedule
##' @param fs, a data frame in flowering schedule format
##' @param opening optional date to specify first date of interest
##' @param closing optional date to specify last date of interest
##' @return a names list with info about the flowering schedule
##' @export
##' @author Stuart Wagenius
##' @examples
##' fs <- generateFS()
##' summaryFS(fs)
##' \dontrun{summaryFS(NULL)}

summaryFS <- function(fs, opening = NULL, closing = NULL) {
  if(is.null(opening)) opening <- min(fs$df[, fs$start])
  if(is.null(closing)) closing <- max(fs$df[, fs$end])
  count <- dim(fs$df)[1]
  season <- seq(opening, closing, 1)
  dailyCount <- integer(length(season))
  for (i in 1:length(season)){
    dailyCount[i] <- sum(season[i] >= fs$df[, fs$start] & season[i] <= fs$df[, fs$end])
  }
  fd <- data.frame(day = season, count = dailyCount)
  peak <- fd[mean(which(fd$count == max(fd$count))), "day"]
  range50 <- c(median(fs$df[ , fs$start]), 
               median(fs$df[ , fs$end]))
  ans <- list(opening = opening, closing = closing, peakDate = peak, range50 = range50, fl.density = fd)
  ans
}
