##' plotMap makes a map of a population
##'
##' A great function 
##' 
##' @title graphical visualization of a spatial disribution
##' @param pp dataframe in 3dpop format, at least with x and y columns
##' @param xlab character label for the x-axis, defaults to "E"
##' @param ylab character label for the y-axis, defaults to "N"
##' @param pch optional plotting character
##' @param ... graphical parameters may also be supplied as arguments
##' @return nothing
##' @return optional arguments for the plot function
##' @export
##' @author Stuart Wagenius
##' @seealso see generic function \code{\link{points}} for values of \code{pch}
##' @examples
##' pop <- generatePop()
##' plotMap(pop)
##' \dontrun{plotMap(NULL)}

plotMap <- function (pp, xlab = "E", ylab = "N", pch = NULL, ...) 
{
  plot.default(pp$df[, pp$x], pp$df[, pp$y], type = "n", xlab = xlab, 
               ylab = ylab, ...)
  if(is.null(pch)) {
    text(pp$df[, pp$x], pp$df[, pp$y], pp$df[, pp$id], ...)
  } else {
    points(pp$df[, pp$x], pp$df[, pp$y], pch = pch, ...)
  }
}

