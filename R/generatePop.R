##' generatePop generates a 3dpop object -- a simulated population in a standard format with individuals randomly assigned a mating schedule, a random location, and S-alleles
##'  
##'
##' A great function 
##' 
##' @title simulate a 3d population
##' @param size integer number of plants
##' @param meanSD date mean start date
##' @param sdSD date standard deviation of start date
##' @param meanDur numeric duration in days
##' @param sdDur standard deviation of duration in days
##' 
##' @param xRange range of spatial extent of individuals along x-axis
##' @param yRange range of spatial extent of individuals along y-axis
##' @param distro unimplemented
##' 
##' @param sAlleles integer count of S-Alleles that could be in the population
##' 
##' 
##' @return 3dpop object -- a data frame with columns identified
##' @export
##' @author Stuart Wagenius
##' @examples
##' generatePop()
##' \dontrun{generatePop(NULL)}

generatePop <- 
function (size = 30, 
          meanSD = "2012-07-12", sdSD = 6, meanDur = 11, sdDur = 3,
          xRange = c(0, 100), yRange = c(0, 100), distro = "unif",
          sAlleles = 10) 
{
    md <- as.Date(strptime(meanSD, "%Y-%m-%d"))
    sd <- md + round(rnorm(size, 0, sdSD), 0)
    ed <- sd + round(rnorm(size, meanDur, sdDur), 0)
    if(distro != "unif") warning("distro must be unif")
    xv <- runif(size, min = xRange[1], max = xRange[2])
    yv <- runif(size, min = yRange[1], max = yRange[2])
    sM <- sample(x = 1:sAlleles, size = size, replace = TRUE)
    sP <- sapply(sM, FUN = function(x) sample((1:sAlleles)[-x], 1))
    df <- data.frame(pla = 1:size, start = sd, end = ed, 
                     x = xv, y = yv, 
                     s1 = sM, s2 = sP)
    make3d(df, start = "start", end = "end")
}

