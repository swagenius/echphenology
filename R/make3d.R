##' make3d edits a dataframe into a 3dpop object with standard format
##'
##' A great function 
##' 
##' @title update a data frame to a 3dpop
##' @param df data frame
##' @param startCol character name of column with start dates
##' @param endCol character name of column with end dates
##' @param xCol character name of column with x or E coordinates
##' @param yCol character name of column with y or N coordinates
##' @param s1Col character name of one column with S-allele
##' @param s2Col character name of another column with S-alleles
##' @param idcode character name for column with unique identifier
##' 
##' @return 3dpop object, a data frame with columns identified
##' @export
##' @author Stuart Wagenius
##' @examples
##' \dontrun{makeFS(NULL)}
##' 
 
make3d <- function (df, 
                    startCol = "sd.E", endCol = "ed.L",
                    xCol = "x", yCol = "y",
                    s1Col = "s1", s2Col = "s2",
                    idcode = "id") 
{
    if(!idcode %in% names(df)) df[ , idcode] <- 1:dim(df)[1] 
    ans <- list(df = df, start = startCol, end = endCol,
                x = xCol, y = yCol, s1 = s1Col, s2 = s2Col,
                id = idcode)
    ans
}
